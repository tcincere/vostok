# Vostok

![Vostok header in a docker container](./imgs/vostok-in-docker-title.png)

Vostok is a script written in POSIX shell and is designed to enumerate container environments (LXC and Docker) and identify any vulnerabilities present on the system. It also is capable of enumerating non-containerised environments, but fairly very generally.

Vostok can also actively exploit any vulnerabilities discovered using the user-friendly help menu invoked by: `./vostok.sh -h`. Vostok can currently exploit five vulnerabilities, and most importantly can break out of a container environment if the necessary configurations are present.

---

### Enumeration

Vostok will perform basic enumeration upon the system by invoking `./vostok.sh -e` This includes, but is not limited to:

- Users

- Hostname

- Groups 

- The distribution

- Shells available

- SETUID binaries 

- Useful tools (such as curl, gcc etc)

- Bash history and environment variables of interest

- Further information depending on the container runtime, such as:
  
  - Containers information (names, running, active, stopped, ip addresses)
  
  - Runtime version
  
  - Whether the runtime is installed
  
  - Security policies (app armor, secomp)

Vostok also can enumerate from a mounted socket and obtain the kernel version, container information and more.

![Vostok enumeration, part 1](./imgs/vostok-enum-1.png)
![Vostok enumeration, part 2](./imgs/vostok-enum-2.png)

---

### Exploitation

Vostok currently has five exploit functions which work consistently. These are:

- `exploitDockerGroup()`
  
  - This exploits unprivileged users who are members of the Docker group. This exploit can provide an unprivileged user a root shell.

- `exploitLXDGroup()`
  
  - Similiar to `exploitDockerGroup()` but for the `lxd` group. This function offers a choice of a root shell, or simply printing the contents of `/etc/shadow`. 

- `exploitMountEntireDisk()`
  
  - This function will exploit a privileged container using Docker or LXC. It exploits the use of `fdisk` and mounts the system disk onto a container, and provides a chroot shell if successful. 

- `exploitCapPtrace()`
  
  - This exploits the `PTRACE` capability given to a Docker container. It uses `gdb` to attach to a `/bin/sh` or `/bin/bash` process and creates a reverse shell; allowing the user to escape from a container.

- `exploitDockerSocket()`
  
  - This exploits a mounted Docker socket typically located at `/var/run/docker.sock`. A new container is created via the UNIX socket which mounts the host file-system to the container. 

---
### Options

Vostok features a user-friendly help menu which describes the certain flags Vostok understands.

![Vostok help menu](./imgs/vostok-help-menu.png)

For more images and examples, head to the /imgs/ directory. 

---
### Inspiration and credit

This project was inspired by a curiosity for containers and a love for the Linux operating system. Inspiration was also gained from other Linux scripts out there, such as `deepce.sh`. 

- [deepce.sh](https://github.com/stealthcopter/deepce)   

- [linpeas.sh](https://github.com/carlospolop/PEASS-ng) 

The commands used to create a container using the UNIX socket were adapted from:

- [PwnPeter](https://gist.github.com/PwnPeter/3f0a678bf44902eae07486c9cc589c25)


