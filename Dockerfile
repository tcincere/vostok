FROM alpine:latest

WORKDIR /

RUN mkdir /host-system    

WORKDIR /tmp

COPY edit-sudoers.sh /tmp/edit-sudoers.sh

RUN chmod u+x /tmp/edit-sudoers.sh

ENTRYPOINT ["./edit-sudoers.sh"]
